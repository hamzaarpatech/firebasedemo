//
//  ViewController.swift
//  firebasedemo
//
//  Created by Hamza on 2/20/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    
    @IBOutlet weak var textLabel: UITextField!
    @IBOutlet weak var selectedImage: UIImageView!
    
    var ref = DatabaseReference.init()
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.ref = Database.database().reference()
        gestureRecognize()
        view.endEditing(true)
    }
    
    func gestureRecognize(){
        let tapGesture = UITapGestureRecognizer()
        selectedImage.isUserInteractionEnabled = true
        selectedImage.addGestureRecognizer(tapGesture)
        tapGesture.addTarget(self, action: #selector(ViewController.openGallery(tapGesture:)))
    }
    
    @objc func openGallery(tapGesture : UITapGestureRecognizer){
        print("browse")
        self.setupImagePicker()
    }

    @IBAction func sendButton(_ sender: UIButton) {
//        let dict = ["name" : "Hamza" , "message" : textLabel.text!]
//        self.ref.child("chat").childByAutoId().setValue(dict)
        self.saveFIrebaseData()
        
    }
    
    func saveFIrebaseData(){
        self.uploadImage(self.selectedImage.image!) { (url) in
            self.saveImage(name: self.textLabel.text!, profileURL: url!) { (success) in
                if success != nil {
                  print("Data sending")
               }
            }
        }
    }
}

extension ViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate{
    
    func setupImagePicker(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let pickimage = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        selectedImage.image = pickimage
        self.dismiss(animated: true, completion: nil)
    }
    
    func uploadImage(_ image :UIImage , completion: @escaping (_ url : URL?)->()){
        let storageRef = Storage.storage().reference().child("myimage.png")
        let imgData =  selectedImage.image?.pngData()
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        storageRef.putData(imgData!, metadata: metaData){ (metaData , error) in
            if error == nil {
                print("success")
                storageRef.downloadURL(completion : { (url, error) in
                    completion(url)
                    })
                }
            else{
                print("error in save message")
                completion(nil)
            }
        }
    }
    
    func saveImage(name: String , profileURL:URL , completion: @escaping ((_ url : URL?)->())){
        print(profileURL.absoluteString)
        let dict = ["name" : "Hamza" , "message" : textLabel.text!, "profileURL":profileURL.absoluteString]
        self.ref.child("chat").childByAutoId().setValue(dict)
    }
}


